console.log("Soal1")
console.log("")
var wereWolf=(nama,peran)=>{
    if(nama===""){
        console.log("Nama harus diisi!")
    }else if(nama!==""&&peran===""){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
    }else if(peran==='Penyihir'){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
    }else if(peran==="Guard"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }else if(peran==="Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
    }else{
        console.log("Halo "+nama+",peran yang kamu input tidak tersedia. Pilih peranmu untuk memulai game!")
    }
}

// Output untuk Input nama = '' dan peran = ''
wereWolf("","")
console.log("")
//Output untuk Input nama = 'Agus' dan peran = ''
wereWolf("Agus","")
console.log("")
//Output untuk Input nama = 'Luki' dan peran 'Penyihir'
wereWolf("Luki","Penyihir")
console.log("")
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
wereWolf("Jenita","Guard")
console.log("")
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
wereWolf("Junaedi","Werewolf")
console.log("")
//Output untuk Input nama = 'Junaedi' dan peran 'tess'
wereWolf("Junaedi","tess")
console.log("")

console.log("Soal2")
var formatTanggal=(hari,bulan,tahun)=>{
    var namabulan=""
    if(hari>31||hari<1){
        console.log("salah input tanggal!!")
    }else if(tahun<1900||tahun>2200){
        console.log("salah input tahun!!")
    }else if(bulan<1||bulan>12){
        console.log("salah input Bulan!!")
    }else{
        switch (bulan) {
            case 1:
                namabulan="Januari"
                break;
            case 2:
                namabulan="Februari"
                break;
            case 3:
                namabulan="Maret"
                break;
            case 4:
                namabulan="April"
                break;
            case 5:
                namabulan="Mei"
                break;
            case 6:
                namabulan="Juni"
                break;
            case 7:
                namabulan="Juli"
                break;
            case 8:
                namabulan="Agustus"
                break;
            case 9:
                namabulan="September"
                break;
            case 10:
                namabulan="Oktober"
                break;
            case 11:
                namabulan="November"
                break;
            case 12:
                namabulan="December"
                break;
            default:
                
                break;
        }

        console.log(hari+" "+namabulan+" "+tahun)
    }

}

console.log("")
formatTanggal(1,5,1990)
console.log("")
formatTanggal(1,12,1995)
console.log("")
formatTanggal(32,12,1995)
console.log("")
formatTanggal(1,12,2201)